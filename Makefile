## ================================================================
## Flex system verification script
## ================================================================

## Dafny flags to run verification with.
## For details, see 'dafny /help'
FLAGS=/vcsLoad:0.5 /timeLimit:240 /compile:0 /warnShadowing /separateModuleOutput
.PHONY:

all: sys flex properties

sys: ./TVF/BaseTypes.dfy ./TVF/TONTypes.dfy ./TVF/TONContract.dfy ./TVF/Queue.dfy ./TVF/MapToSeq.dfy
	dafny $(FLAGS) $?

flex: ./Flex/FlexClient.dfy ./Flex/TradingPair.dfy ./Flex/XchgPair.dfy ./Flex/Price.dfy ./Flex/PriceXchg.dfy ./Flex/RootTokenContract.dfy ./Flex/TONTokenWallet.dfy
	dafny $(FLAGS) $?

properties: ./Flex/PROP_SEC01.dfy ./Flex/PROP_SEC02.dfy ./Flex/PROP_SEC03.dfy ./Flex/PROP_SEC04.dfy
	dafny $(FLAGS) $?
