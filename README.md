FLeX Formal Verification Phase 2 artifacts
------------------------------------------

This directory  contains artifacts  for second  phase of  FLeX trading
system formal verification, performed by Evgeniy Shishkin.

# Files

The  files  in  TVF  directory  constitutes what  is  now  called  TON
Verification Framework.

The high-level specification for Flex is located in FlexSpec directory.

The  following files  in Flex  directory  are related  solely to  Flex
verification.

# Installation

To run verification, do the following:

1) Install Dafny version 3.3.0 as stated here:
https://github.com/dafny-lang/dafny/wiki/INSTALL

2) In the FlexPhase2 directory, run:
> make

